Dado('que faço um DELETE na API') do
  steps %(
    Dado que faço um GET na API a
    Então recebo um unico contato
  )

  log $id_contato_unico

  @url = "https://api-de-tarefas.herokuapp.com/contacts/#{$id_contato_unico}"

  @headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/vnd.tasksmanager.v2'
  }

  @response = HTTParty.delete(@url, headers: @headers)
  @response_valida = HTTParty.get(@url, headers: @headers)
end

Então('eu listo o contato deletado') do
  log @response
  raise "Erro: o codido retornado esta errado!" if @response.code != 204
  raise "Erro: o codido retornado esta errado!" if @response_valida.code != 404
end