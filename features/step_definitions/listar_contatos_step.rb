Dado('que faço um GET na API') do
  @url = 'https://api-de-tarefas.herokuapp.com/contacts/'
  @headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/vnd.tasksmanager.v2'
  }
  @response = HTTParty.get(@url, headers: @headers)
end

Então('recebo um resultado') do
  raise "Erro: o codido retornado esta errado!" if @response.code != 200
  raise "Erro: A api esta retornando vario" if @response["data"] == nil
end