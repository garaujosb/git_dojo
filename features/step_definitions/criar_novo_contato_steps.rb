Dado('que faço um POST na API') do
  @url = 'https://api-de-tarefas.herokuapp.com/contacts/'
  @headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/vnd.tasksmanager.v2'
  }
  @body = {
    "name": "Naldo Vilariano",
    "email": Faker::Internet.email,
    "age": "29",
    "phone": "11952525252"
  }.to_json
  @response = HTTParty.post(@url, headers: @headers, body: @body)
end

Então('eu listo o contato criado') do
  log @response
  raise "Erro: o codido retornado esta errado!" if @response.code != 201
  raise "Erro: A api esta retornando vario" if @response["data"] == nil
end