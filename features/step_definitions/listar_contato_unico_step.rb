Dado('que faço um GET na API a') do
  @url = 'https://api-de-tarefas.herokuapp.com/contacts/'
  @headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/vnd.tasksmanager.v2'
  }
  @response = HTTParty.get(@url, headers: @headers)
end

Então('recebo um unico contato') do
  log @response["data"][0]
  raise "Erro: o codido retornado esta errado!" if @response.code != 200
  raise "Erro: A api esta retornando vario" if @response["data"] == nil
  $id_contato_unico = @response["data"][0]["id"]
end