# Software e Configuração:
- Neste projeto estou fazendo automação de testes web usando [Ruby](https://www.ruby-lang.org/pt/) como linguagem de programação, [Cucumber](https://cucumber.io/) para interpretação de Scenarios, Steps e construção da estrutura, como intermediador para conexão em API`s estou usando o [HTTParty](https://github.com/jnunemaker/httparty).
    - Não estou especificando versões para minhas Gems
    - Minhas declaraçoes de dependencias estão no arquivo Gemfile

# Documentação API
- [Link Documentação](https://apidetarefas.docs.apiary.io/#reference/0/contatos/apagar-contato)

# Sobre a Automação
    - A automação hoje está separado por cada funcionalidade, POST, GET, PUT, DELETE.

# Detalhes para os tipos de requisições:
## POST
* `POST` - É o método para CRIAR uma determinada informação na API.
## GET
* `GET` - É o método usado para BUSCAR uma determinada informação na API.
## PUT
* `PUT` - É o método para ALTERAR uma determinada informação na API.
## DELETE
* `DELETE` - É o método para EXCLUIR uma determinada informação na API.

# Códigos de retorno HTTP da API:
* `200 = OK` - O recurso solicitado foi processado e retornado com sucesso.
* `201 = Created` - O recurso informado foi criado com sucesso.
* `401 = Unauthorized` - A chave da API está desativada, incorreta ou não foi informada corretamente. Consulte a seção sobre autenticação da documentação.
* `402 = Payment Required` - A chave da API está correta, porém a conta foi bloqueada por inadimplência. Neste caso, acesse o painel para verificar as pendências.
* `403 = Forbidden` - O acesso ao recurso não foi autorizado. Este erro por ocorrer por dois motivos: (1) Uma conexão sem criptografia foi iniciada. Neste caso utilize sempre HTTPS. (2) As configurações de perfil de acesso não permitem a ação desejada. Consulte as configurações de acesso no painel de administração.
* `404 = Not Found` - O recurso solicitado ou o endpoint não foi encontrado.
* `406 = Not Acceptable` - O formato enviado não é aceito. O cabeçalho Content-Type da requisição deve contar obrigatoriamente o valor application/json para requisições do tipo POST e PUT.
* `422 = Unprocessable Entity` - A requisição foi recebida com sucesso, porém contém parâmetros inválidos. Para mais detalhes, verifique o atributo errors no corpo da resposta.
* `429 = Too Many Requests` - O limite de requisições foi atingido. Verifique o cabeçalho Retry-After para obter o tempo de espera (em segundos) necessário para a retentativa.
* `400 = Bad Request` - Não foi possível interpretar a requisição. Verifique a sintaxe das informações enviadas.
* `500 = Internal Server Error` - Ocorreu uma falha na plataforma Vindi. Por favor, entre em contato com o atendimento.

# Validações de cada Etapa:
## Status Code  - A automação valida o status code, depois de cada batida no serviço
    - Valida se está retornando algum serviço
    - Valida status code == 200
    
## Comando para executar testes exemplos:
    - bundle exec cucumber

## ------------------------- Exercicios 2 -------------------------

* `1` - Criar automação para fluxo completo (Regressivo!!)

* `Dica:`
    * `1` - Passo 1 Criar o usuário
    * `2` - Passo 2 Consultar o contato criado
    * `3` - Passo 3 Editar o contato criado
    * `4` - Passo 4 Deletar o contato criado
## ------------------------- Exercicios 2 -------------------------