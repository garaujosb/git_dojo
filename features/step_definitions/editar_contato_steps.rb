Dado('que faço um PUT na API') do

  steps %(
    Dado que faço um GET na API a
    Então recebo um unico contato
  )

  log $id_contato_unico

  @url = "https://api-de-tarefas.herokuapp.com/contacts/#{$id_contato_unico}"
  @headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/vnd.tasksmanager.v2'
  }
  @body = {
    "name": "Nome Alterado Por Bot",
    "email": Faker::Internet.email,
    "age": "30",
    "phone": "11963636363"
  }.to_json

  @response = HTTParty.put(@url, headers: @headers, body: @body)
  
end

Então('eu listo o contato editado') do
  log @response
  raise "Erro: o codido retornado esta errado!" if @response.code != 200
  raise "Erro: A api esta retornando vario" if @response["data"] == nil
end